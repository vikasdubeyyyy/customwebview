package com.customwebview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.PopupMenu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityWebView extends AppCompatActivity {
    private static final String TAG = ActivityWebView.class.getSimpleName();

    private AppCompatActivity activity;
    private Context context;
    private TextView pageTitle, pageURL;
    private WebView webView;
    private ProgressBar progressBar;
    private String URL = "https://www.google.co.in/";
    protected int key;

    private AppCompatImageButton close, more, back, forward;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        activity = this;
        context = this;
        pageTitle = (TextView) findViewById(R.id.pageTitle);
        pageURL = (TextView) findViewById(R.id.pageURL);
        webView = (WebView) findViewById(R.id.webView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        close = (AppCompatImageButton) findViewById(R.id.close);
        more = (AppCompatImageButton) findViewById(R.id.more);
        back = (AppCompatImageButton) findViewById(R.id.back);
        forward = (AppCompatImageButton) findViewById(R.id.forward);
        progressBar.setMax(100);

        webView.setWebChromeClient(new CustomWebChromeClient());
        webView.setWebViewClient(new CustomWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(URL);

        progressBar.setVisibility(View.VISIBLE);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(v.getContext(), v);
                popup.inflate(R.menu.menu_web_view);
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getItemId() == R.id.action_refresh) {
                            webView.reload();
                        }

                        if (item.getItemId() == R.id.action_share) {
                            Intent sendIntent = new Intent();
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.putExtra(Intent.EXTRA_TEXT, webView.getUrl());
                            sendIntent.setType("text/plain");
                            activity.startActivity(Intent.createChooser(sendIntent, "Share Via"));
                        }

                        if (item.getItemId() == R.id.action_copy_link) {
                            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
                            android.content.ClipData clip = android.content.ClipData.newPlainText("Link Copied", webView.getUrl());
                            clipboard.setPrimaryClip(clip);
                            Toast.makeText(activity, "Link Copied", Toast.LENGTH_SHORT).show();
                        }

                        if (item.getItemId() == R.id.action_open_with) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(webView.getUrl()));
                            startActivity(browserIntent);
                        }
                        return false;
                    }
                });
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goBack();
            }
        });

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.goForward();
            }
        });
    }

    public class CustomWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            BroadCastManager.onPageStarted(context, key, url);
            super.onPageStarted(view, url, favicon);
            progressBar.setProgress(0);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            BroadCastManager.onPageFinished(context, key, url);

            pageTitle.setText(view.getTitle());
            pageURL.setText(view.getUrl());

            progressBar.setVisibility(View.GONE);
            progressBar.setProgress(100);

            if (view.canGoBack()) {
                back.setVisibility(View.VISIBLE);
            } else {
                back.setVisibility(View.GONE);
            }

            if (view.canGoForward()) {
                forward.setVisibility(View.VISIBLE);
            } else {
                forward.setVisibility(View.GONE);
            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            BroadCastManager.onLoadResource(context, key, url);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            BroadCastManager.onPageCommitVisible(context, key, url);
        }
    }

    public class CustomWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int progress) {
            BroadCastManager.onProgressChanged(context, key, progress);

            if (progress == 100) progress = 0;
            progressBar.setProgress(progress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            BroadCastManager.onReceivedTitle(context, key, title);
        }

        @Override
        public void onReceivedTouchIconUrl(WebView view, String url, boolean precomposed) {
            BroadCastManager.onReceivedTouchIconUrl(context, key, url, precomposed);
        }
    }

    public boolean canGoBack() {
        return webView != null && webView.canGoBack();
    }

    public void goBack() {
        if (webView != null) {
            webView.goBack();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK && canGoBack()) {
            goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (!webView.canGoBack()) {
            finish();
        } else {
            webView.goBack();
        }
    }
}
